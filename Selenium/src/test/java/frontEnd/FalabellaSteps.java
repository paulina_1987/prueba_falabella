package frontEnd;

import Utils.Helpers;
import Utils.Start;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static Utils.Helpers.TakeEvidence;

public class FalabellaSteps extends FalabellaObjects {
    public static WebDriver driver;
    public static String nameClass;

    public FalabellaSteps() {
        driver = Start.getInstance().getDriver();
        nameClass = getClass().getSimpleName();
    }

    @Given("^Ingreso a Falabella$")
    public void ingreso_a_Falabella() {
        driver.get("https://www.falabella.com/falabella-cl/");
    }

    @When("^Presionar boton Categorias$")
    public void presionar_boton_Categorias(){
        Helpers.waitBySeconds(driver, 10);
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", botonCategorias(driver));
        Helpers.waitBySeconds(driver, 5);
    }


    @When("^Seleccionar menu Tecnologias$")
    public void seleccionar_menu_Tecnologias(){
        Helpers.waitBySeconds(driver, 3);
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", opcionTecnologia(driver));
    }

    @When("^Seleccionar submenu Consolas$")
    public void seleccionar_submenu_Consolas(){
        Helpers.waitBySeconds(driver, 3);
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", opcionConsolas(driver));
    }

    @When("^Selecciono la marca ([^\"]*)$")
    public void selecciono_la_marca(String idMarca){
        WebElement check = checkboxMarca(driver,idMarca);
        Helpers.waitBySeconds(driver, 2);
        TakeEvidence(driver,"Seleccion Marca",nameClass);
        Helpers.checkSelect(driver,check,idMarca);
    }

    @When("^Selecciono el producto ([^\"]*)$")
    public void selecciono_el_producto(String producto) {
        Helpers.waitBySeconds(driver, 10);
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", seleccionProducto(driver,producto));
        Helpers.waitBySeconds(driver, 5);
        TakeEvidence(driver,"Seleccion Producto",nameClass);
    }

    @When("^Presiono el boton Agregar al a Bolsa$")
    public void presionoElBotonAgregarAlABolsa() {
        Helpers.waitBySeconds(driver, 3);
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", botonAgregarBolsa(driver));
    }

    @When("^Selecciono la cantidad del producto ([^\"]*)$")
    public void selecciono_la_cantidad_del_producto(int cantidad){

        for(int n = 0; n<cantidad;n++) {
            ((JavascriptExecutor) driver).executeScript("arguments[0].click()", seleccionarCantidad(driver, "M"));
            Helpers.waitBySeconds(driver, 3);
            TakeEvidence(driver,"Productos Añadidos_"+n,nameClass);
        }


    }

    @Then("^Presiono el boton Bolsa de Compras ([^\"]*) y se cambia la pantalla$")
    public void debo_ver_la_cantidad_marcada_en_pantalla(String tituloBolsaCompras) {
        Helpers.waitBySeconds(driver, 5);
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", botonBolsaCompras(driver));
        Helpers.waitBySeconds(driver, 5);
        TakeEvidence(driver,"Pantalla Bolsa de Compras",nameClass);
        String tituloActual = textoBolsaCompras(driver).getText();
        Assert.assertEquals(tituloActual,tituloBolsaCompras);
    }
}
