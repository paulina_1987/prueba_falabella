@BusquedaConsolas
Feature: Buscar Tecnologia en Falabella

  Background:  Ingreso en el sitio
    Given Ingreso a Falabella

  Scenario Outline: Login
    When Presionar boton Categorias
    And Seleccionar menu Tecnologias
    And Seleccionar submenu Consolas
    And Selecciono la marca <marca>
    And Selecciono el producto <producto>
    And Presiono el boton Agregar al a Bolsa
    And Selecciono la cantidad del producto <cantidad>
    Then Presiono el boton Bolsa de Compras <cantidadReal> y se cambia la pantalla

    Examples:
      |   marca     ||producto   ||cantidad || cantidadReal      |
      |  NINTENDO-5 ||  8014967  ||  2      ||  Bolsa de Compras |
      |  NINTENDO-5 ||  8014966  ||  2      ||  Bolsa de Compras |

