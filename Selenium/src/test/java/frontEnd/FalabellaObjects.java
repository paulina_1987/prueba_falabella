package frontEnd;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FalabellaObjects {


    public static WebElement botonCategorias(WebDriver driver){
        return  driver.findElement(By.className("HamburgerIcon_hamburgerText__3_7OL"));
    }


    public static WebElement opcionTecnologia(WebDriver driver){
        return  driver.findElement(By.className("FirstLevelItems_menuText__UYB9A"));
    }

    public static WebElement opcionConsolas(WebDriver driver){
         return  driver.findElement(By.xpath("//a[contains(text(),'Consolas')]") );
    }

    public static WebElement checkboxMarca(WebDriver driver,String marca){
        return  driver.findElement(new By.ByCssSelector("input[id='"+marca+"']"));
    }

    public static WebElement seleccionProducto(WebDriver driver,String codigoProducto){
        return  driver.findElement(new By.ByCssSelector("button[id='testId-Pod-action-"+codigoProducto+"']"));
    }

    public static WebElement botonAgregarBolsa(WebDriver driver){
        return  driver.findElement(new By.ByCssSelector("button[class='jsx-1816208196 button button-primary button-primary-xtra-large']"));
    }

    public static WebElement seleccionarCantidad(WebDriver driver, String idText) {
        return driver.findElement(new By.ByCssSelector("button[class='jsx-635184967 increment']"));
    }

    public static WebElement botonBolsaCompras(WebDriver driver){
        return driver.findElement(new By.ByCssSelector("a[class='jsx-3882044876 button button-primary button- ']"));
    }
    public static WebElement textoBolsaCompras(WebDriver driver){
        return driver.findElement(new By.ByCssSelector("span[class='fbra_text fbra_text fbra_checkoutDeliverySteps__yourBasketStepText fbra_selected']"));
    }

}
