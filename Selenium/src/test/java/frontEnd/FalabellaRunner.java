package frontEnd;


import com.cucumber.listener.ExtentProperties;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/frontEnd",
        tags = {"@BusquedaConsolas"},
        plugin = {"com.cucumber.listener.ExtentCucumberFormatter:"})

public class FalabellaRunner {

    @BeforeClass
    public static void setuppre() {

        DateFormat dateFormat = new SimpleDateFormat("YYYYMMDD_HHmmss");
        Date date = new Date();
        String dateG = dateFormat.format(date);

        ExtentProperties extentProperties = ExtentProperties.INSTANCE;
        extentProperties.setReportPath("output/BusquedaConsolas-" + dateG + ".html");
    }




    @AfterClass
    public static void closeTest() {
    //FalabellaSteps.driver.close();
    }
}
