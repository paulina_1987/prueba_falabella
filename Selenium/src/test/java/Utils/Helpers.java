package Utils;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Helpers {

    public static void waitBySeconds(WebDriver driver, int timeout){
        driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
    }

    public static void TakeEvidence(WebDriver driver,String nombreArchivo, String nombreClase){

        String curDir = System.getProperty("user.dir");
        File directory = new File((curDir + "/imagenes/" + nombreClase));
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
        Date date = new Date();
        String dateEvidence = dateFormat.format(date);

        try {
            FileUtils.copyFile(scrFile, new File(directory.getAbsolutePath()+ "/" + nombreArchivo + "_" + dateEvidence + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void checkSelect(WebDriver driver, WebElement check, String checkId) {
        ((JavascriptExecutor) driver).executeScript("document.querySelector(\"#"+checkId+"\")",check);
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", check);
    }
}
