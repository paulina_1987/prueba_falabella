package Utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Start {
    private WebDriver driver;
    private static Start startDriver = null;

    public Start() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver(setChromeOptions());
    }

    public ChromeOptions setChromeOptions() {
        ChromeOptions options = new ChromeOptions();
        options.setPageLoadStrategy(PageLoadStrategy.EAGER);
        options.addArguments("start-maximized");
        return options;
    }

    public static Start getInstance() {
        if (startDriver == null) {
            startDriver = new Start();
        }
        return startDriver;
    }

    public WebDriver getDriver() {
        return driver;
    }




}
